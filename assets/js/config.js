var Appconfig = {
  "version": 1.23,
  "items": [
    {
      "name": "web",
      "label": "Website",
      "url": "http://www.multipolar.com/",
      "menulist": [],
      "ui": {
        "icon_html": "<ion-icon name='home-outline'></ion-icon>",
        "box_class": "bg-yellow",
      }
    },
    {
      "name": "old-project",
      "label": "Old Project",
      "url": "",
      "menulist": [],
      "ui": {
        "icon_html": "<i class='fa fa-server'></i>",
        "box_class": "bg-aqua",
      },
      "hide": true
    },
    {
      "name": "project-server",
      "label": "Project Server",
      "url": "https://mlpt-km.multipolar.com/pwa",
      "menulist": [
        {
          "label": "PS Menu Pertamax",
          "url": "http://google.com",
        },
        {
          "label": "Menu Keduax",
          "url": "PS http://google-2.com",
        },
        {
          "label": "PS Menu Ketigax",
          "url": "http://google-3.com",
        },
      ],
      "ui": {
        "icon_html": "<i class='fa fa-server'></i>",
        "box_class": "bg-aqua",
      }
    },
    {
      "name": "project-costing",
      "label": "Project Costing",
      "url": "https://mlpt-km.multipolar.com/pwa",
      "menulist": [
        {
          "label": "Website",
          "url": "http://google.com",
        },
        {
          "label": "Menu ",
          "url": "PS http://google-2.com",
        },
        {
          "label": "PS Menu",
          "url": "http://google-3.com",
        },
      ],
      "ui": {
        "icon_html": '<i class="fa fa-dollar-sign"></i>',
        "box_class": "bg-aqua",
      }
    },
    {
      "name": "legal",
      "label": "Legal",
      "url": "https://mlpt-km.multipolar.com/pwa",
      "menulist": [
        {
          "label": "Website",
          "url": "http://google.com",
        },
        {
          "label": "Menu ",
          "url": "PS http://google-2.com",
        },
        {
          "label": "PS Menu",
          "url": "http://google-3.com",
        },
      ],
      "ui": {
        "icon_html": "<i class='fas fa-file-contract'></i>",
        "box_class": "bg-aqua",
      }
    },
    {
      "name": "internal",
      "label": "Internal",
      "url": "https://mlpt-km.multipolar.com/pwa",
      "menulist": [],
      "ui": {
        "icon_html": '<i class="fas fa-headset"></i>',
        "box_class": "bg-aqua",
      }
    },
    {
      "name": null,
    },
    {
      "name": "forms",
      "label": "Forms",
      "url": "https://mlpt-km.multipolar.com/pwa",
      "menulist": [
        {
          "label": "Website",
          "url": "http://google.com",
        },
        {
          "label": "Menu ",
          "url": "PS http://google-2.com",
        },
        {
          "label": "PS Menu",
          "url": "http://google-3.com",
        },
      ],
      "ui": {
        "icon_html": '<i class="fas fa-align-left"></i>',
        "box_class": "bg-aqua",
      }
    },
    {
      "name": "E-Requisition",
      "label": "E-Requisition",
      "url": "https://mlpt-km.multipolar.com/pwa",
      "menulist": [
        {
          "label": "Billing Requisition",
          "url": "https://mlptweb.multipolar.com/newbillreq/BillReqIndex.aspx",
        },
        {
          "label": "Billing Requisition non PID ",
          "url": "PS http://google-2.com",
        },
        {
          "label": "Business Travel Assignment & Settlement",
          "url": "http://google-3.com",
        },
        {
          "label": "Delivery Order",
          "url": "http://google.com",
        },
        {
          "label": "Document Numbering",
          "url": "PS http://google-2.com",
        },
        {
          "label": "Exception of Vendor Selection",
          "url": "http://google-3.com",
        },   
        {
          "label": "Vendor Evaluation",
          "url": "http://google-3.com",
        },
        {
          "label": "Tool/Equipment Requisition",
          "url": "http://google.com",
        },
        {
          "label": "Knowledge Acquisition & Settlement",
          "url": "PS http://google-2.com",
        },
        {
          "label": "Payment Request",
          "url": "http://google-3.com",
        },
        {
          "label": "Sales Order Requisition",
          "url": "http://google-3.com",
        },   
        {
          "label": "Stock Requisition",
          "url": "http://google-3.com",
        },
        {
          "label": "Sub-Contractor Requisition",
          "url": "http://google.com",
        },
        {
          "label": "Supplies Requisition",
          "url": "PS http://google-2.com",
        },
        {
          "label": "Tracking",
          "url": "http://google-3.com",
        },
        {
          "label": "Legal Documents",
          "url": "http://google.com",
        },
        {
          "label": "Logbook Contract",
          "url": "PS http://google-2.com",
        },
        {
          "label": "Legal Contract Review",
          "url": "http://google-3.com",
        },
      ],
      "ui": {
        "icon_html": '<i class="fa fa-edit"></i>',
        "box_class": "bg-aqua",
      }
    },
    {
      "name": "CRM",
      "label": "CRM",
      "url": "https://mlpt-km.multipolar.com/pwa",
      "menulist": [
        {
          "label": "Website",
          "url": "http://google.com",
        },
        {
          "label": "Menu ",
          "url": "PS http://google-2.com",
        },
        {
          "label": "PS Menu",
          "url": "http://google-3.com",
        },
      ],
      "ui": {
        "icon_html": '<i class="fas fa-align-left"></i>',
        "box_class": "bg-aqua",
      }
    },
    {
      "name": "COC",
      "label": "Code Of Conduct",
      "url": "https://mlpt-km.multipolar.com/pwa",
      "menulist": [],
      "ui": {
        "icon_html": '<i class="far fa-bookmark"></i>',
        "box_class": "bg-aqua",
      }
    },
    {
      "name": "ESS",
      "label": "ESS",
      "url": "https://mlpt-km.multipolar.com/pwa",
      "menulist": [],
      "ui": {
        "icon_html": '<i class="far fa-bookmark"></i>',
        "box_class": "bg-aqua",
      }
    },
    {
      "name": null,
    },
    {
      "name": "Procurement",
      "label": "Procurement",
      "url": "https://mlpt-km.multipolar.com/pwa",
      "menulist": [],
      "ui": {
        "icon_html": '<i class="fas fa-store-alt"></i>',
        "box_class": "bg-aqua",
      }
    },
    {
      "name": "Sunfish",
      "label": "Sunfish",
      "url": "https://mlpt-km.multipolar.com/pwa",
      "menulist": [],
      "ui": {
        "icon_html": '<i class="fas fa-fingerprint"></i>',
        "box_class": "bg-aqua",
      }
    },
    {
      "name": "CRMO",
      "label": "CRM Online",
      "url": "https://mlpt-km.multipolar.com/pwa",
      "menulist": [],
      "ui": {
        "icon_html": '<i class="fas fa-fingerprint"></i>',
        "box_class": "bg-aqua",
      }
    },
    {
      "name": "KMS",
      "label": "KMS",
      "url": "https://mlpt-km.multipolar.com/pwa",
      "menulist": [],
      "ui": {
        "icon_html": '<i class="fas fa-book-reader"></i>',
        "box_class": "bg-aqua",
      }
    },
  ],
  "config": {
    "item_per_row": 6,
    "allow_crud_menu": false,
  }
};
